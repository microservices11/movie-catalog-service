package com.example.moviecatalogservice.settings;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Configuration //to create as a bean
@ConfigurationProperties("db") //lookup in the properties file for the prefix 'db'
public class DbSettings {

    private String connection;
    private String host;
    private int port;
}
