package com.example.moviecatalogservice.resources;

import com.example.moviecatalogservice.dto.MovieListDto;
import com.example.moviecatalogservice.dto.UserRatingDto;
import com.example.moviecatalogservice.model.CatalogItem;
import com.example.moviecatalogservice.model.Movie;
import com.example.moviecatalogservice.settings.DbSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import javax.sql.DataSource;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/catalog")
public class MovieCatalogResource {

   // private static Logger logger = new LoggerFactory.getLogger(DataSource.class);

    @Autowired
    private DbSettings dbSettings;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private WebClient.Builder webClientBuilder;

    //Possible to retrieve instances of a particular client
//    @Autowired
//    private DiscoveryClient discoveryClient;


    @GetMapping("/greetings")
    public String getGreeting(){

        String db = dbSettings.getConnection() + "___" + dbSettings.getHost()+"___" + dbSettings.getPort();
        return db;
    }

    @GetMapping("/{userId}")
    public List<CatalogItem> getCatalog(@PathVariable("userId") String userId){

        //get all movie ID's
        UserRatingDto userRating = restTemplate.getForObject("http://rating-data-service/ratingsdata/user/"+userId , UserRatingDto.class);
        //for each movie id, call movie info service and get the movie details
        return userRating.getUserRating().stream().map(rating -> {
            Movie movie = restTemplate.getForObject("http://movie-info-service/movies/" + rating.getMovieId(), Movie.class);
            return new CatalogItem(movie.getName(), "alienated", rating.getRating());
        }).collect(Collectors.toList());

        //put them together

    }

    @GetMapping("/")
    public MovieListDto getAllCatalog(){
        MovieListDto movieList = restTemplate.getForObject("http://movie-info-service/movies/", MovieListDto.class);
        return movieList;
    }

    @PostMapping("/post/")
    public String AddMovie(@RequestBody Movie movie){
        return "Successfully added";
    }
}

//WebClient implementation
//            Movie movie = webClientBuilder.build()
//                    .get()
//                    .uri("http://localhost:8082/movies/" + rating.getMovieId())
//                    .retrieve()
//                    .bodyToMono(Movie.class)
//                    .block();
