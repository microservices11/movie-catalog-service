package com.example.moviecatalogservice.dto;

import com.example.moviecatalogservice.model.Movie;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class MovieListDto {
    private List<Movie> movieList;
}
